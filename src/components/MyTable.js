
import { Space, Table, InputNumber, Select,Typography } from 'antd';
import React from 'react';
import 'antd/dist/antd.css';
import { useSelector } from 'react-redux';
import { deleteRow, editRow } from '../actions';
import store from '../store';
import { useState, useEffect } from 'react';
const { Option } = Select;
const { Title } = Typography;

const columns = [
  {
    title: 'operand',
    dataIndex: 'operand',
    key: 'operand',
    render: (_, record) =>
      <Select defaultValue={record?.operand} style={{ width: 120 }} disabled={record.disable} onChange={(val) => handleChange(val, record)}>
        <Option value="-">-</Option>
        <Option value="+">+</Option>
      </Select>,
  },
  {
    title: 'value',
    dataIndex: 'value',
    key: 'value',
    render: (_, record) => <InputNumber min={1} max={1000} defaultValue={0} disabled={record.disable} onChange={(val) => onChange(val, record)} />
  },
  {
    title: 'action',
    key: 'action',
    render: (_, record) => (
      <Space size="middle">
        <a onClick={() => disable(record)}>{record.disable ? "enable" : "Disable"}</a>
        <a onClick={() => store.dispatch(deleteRow(record?.key))} >Delete</a>
      </Space>
    ),
  },
];
const onChange = (val, record) => {
  const new_record = { ...record, value: val ? val : 0 }
  store.dispatch(editRow(new_record))
}

const handleChange = (val, record) => {
  const new_record = { ...record, operand: val }
  store.dispatch(editRow(new_record))
}

const disable = (record) => {
  const new_record = { ...record, disable: !record.disable }
  store.dispatch(editRow(new_record))
}

function MyTable() {
  const myState = useSelector((state) => state.calculator)
  const [total, setTotal] = useState(0)
  useEffect(()=>{
    setTotal(0);
    myState?.rows?.map( ({ value, operand, disable }) => {
      if ( operand === '+' && value && !disable ) setTotal( total=> total + value ) 
      else if ( operand === '-' && value && !disable ) setTotal( total=> total - value )
    },[myState])
  })
  return (
    <>
      <Table columns={columns}
        dataSource={myState.rows}
        pagination={false}
      />
      <Title level={2}>Total: { total} </Title>
    </>
  );
}

export default MyTable;
