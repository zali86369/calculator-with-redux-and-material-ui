export const addRow = ( key ) => {
    return {
        type: 'ADD_ROW',
        payload: {
            operand: '-',
            value: 0,
            disable: false,
            key
        }
    }
}
export const editRow = (payload) => {
    return {
        type: 'EDIT_ROW',
        payload
    }
}
export const deleteRow = (key) => {
    return {
        type: 'DELETE_ROW',
        payload: {
            key
        }
    }
}
export const setTotal = () => {
    return {
        type: 'SET_TOTAL',
    }
}
export const uniqueKey = () => {
    return {
        type: 'UNIQUE_KEY',
    }
}
