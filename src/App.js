
import './App.css';
import React from 'react';
import MyTable from './components/MyTable';
import { Typography, Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons'
import { useSelector } from 'react-redux';
import { addRow, uniqueKey } from './actions/index'
import store from './store';
const { Title } = Typography;



function App() {
  const myState = useSelector( (state) => state.calculator )
  console.log(myState)
  const addNewRow = ( e) => {
    e.preventDefault();
    store.dispatch( addRow( myState.uniqueKey ) )
    store.dispatch( uniqueKey() )
  }
  return (
    <div className='App' >
      <Title underline level={1}>Calculator Test</Title>
      <Button type="primary" onClick={(e)=>addNewRow(e)} icon={<PlusOutlined />} >ADD ROW</Button>
      <MyTable />
    </div>
  );
}

export default App;
