import calculator from "./calculator";
import { combineReducers } from "redux";

const rootReducer = combineReducers( {
    calculator,
})

export default rootReducer