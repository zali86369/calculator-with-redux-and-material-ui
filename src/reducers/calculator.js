
const initialState ={
    rows:[],
    total: 0,
    uniqueKey: 0
}

const calculator = ( state = initialState, action ) => {
    switch(action.type){
        case "ADD_ROW": 
        return {
            ...state,
            rows:[...state.rows, action.payload]
        }
        case "EDIT_ROW": 
        return { 
            ...state, 
            rows: state.rows.map(
                (content) => content.key === action.payload.key ?  action.payload: content
            ),
         }
        case "DELETE_ROW": 
        return {
            ...state,
            rows: state.rows.filter(item => item.key !== action.payload.key),
          }
        // this will also work if we use for setting total  
        case "SET_TOTAL": 
            state.total = 0
            state.rows.map( ({ value, operand, disable })=> {
                if ( operand === '+' && value && !disable ) state.total = state.total + value 
                else if ( operand === '-' && value && !disable ) state.total = state.total - value
            } )
        case "UNIQUE_KEY": 
        return {
            ...state, uniqueKey: state.uniqueKey+1
        }
        default: return state 
    }
}

export default calculator